CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Installation and Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

Rest block data modules intended to accomplish maximum Drupal’s potential in the
fully decoupled applications.

It helps developers to design the decoupled applications layout from the 
Drupal’s block layout and expose layout structure along with block data through 
REST resources.


REQUIREMENTS
------------

 * [rest_block_layout](https://www.drupal.org/project/rest_block_layout)
 * [menu_normalizer](https://www.drupal.org/project/menu_normalizer)


INSTALLATION
------------

Install the "Rest block data" module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


INSTALLATION AND CONFIGURATION
------------------------------
    
    1. Install rest_block_layout and menu_normalizer, rest_block_data.
    2. Enable rest_block_data module and it’s dependencies.
    3. Enable Block Layout resource (You may need RestUI).
    4. Grant required access for Block Layout resource in people -> permissions.
    5. Access the block layout data https://yourdomain.com/block-layout?_format=json&path=/


TROUBLESHOOTING
------------------------------

When you couldn’t see the data attribute in the block object which means that 
block doesn’t register for rest block data. 
Please refer hook_block_alter() and Drupal\rest_block_data\Plugin\Block\SystemBrandingBlock 
for how to register a block for 
rest block data by Drupal\rest_block_data\Plugin\Block\BlockInterface interface.

When you get Could not normalize object of type “X”, no supporting normalizer 
found error which means there is no normalizer for that type.


MAINTAINERS
-----------

 * Karuppusamy Ganesan - https://www.drupal.org/u/gkaruppusamy
