<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\views\Plugin\Block\ViewsBlock as ParentBlock;

/**
 * Override \Drupal\views\Plugin\Block\ViewsBlock to include data for the REST.
 */
class ViewsBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return $this->view;
  }

}
