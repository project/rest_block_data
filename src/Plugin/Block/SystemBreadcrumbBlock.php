<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\system\Plugin\Block\SystemBreadcrumbBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class SystemBreadcrumbBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return $this->breadcrumbManager->build($this->routeMatch);
  }

}
