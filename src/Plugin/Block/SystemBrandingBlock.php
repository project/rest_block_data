<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\system\Plugin\Block\SystemBrandingBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class SystemBrandingBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    $site_config = $this->configFactory->get('system.site');

    return [
      'site_logo'   => theme_get_setting('logo.url'),
      'site_name'   => $site_config->get('name'),
      'site_slogan' => $this->configuration['use_site_slogan'],
    ];
  }

}
