<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\Core\Block\Plugin\Block\PageTitleBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class PageTitleBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return $this->title;
  }

}
