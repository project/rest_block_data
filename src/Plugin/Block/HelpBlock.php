<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\help\Plugin\Block\HelpBlock as ParentBlock;

/**
 * Override \Drupal\help\Plugin\Block\HelpBlock to include data for REST.
 */
class HelpBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return $this->build();
  }

}
