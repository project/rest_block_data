<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\system\Plugin\Block\SystemMessagesBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class SystemMessagesBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    \Drupal::messenger()->addStatus('some test message');

    return [
      'messages' => \Drupal::messenger()->deleteAll(),
      'status_headings' => [
        'status' => $this->t('Status message')->render(),
        'error' => $this->t('Error message')->render(),
        'warning' => $this->t('Warning message')->render(),
      ],
    ];
  }

}
