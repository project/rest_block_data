<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\system\Plugin\Block\SystemMainBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class SystemMainBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    // Get the current request.
    $request = \Drupal::request();

    if ($entity = $request->attributes->get('_block_layout_entity')) {
      if ($access = $request->attributes->get('_block_layout_access')) {
        if ($access->isAllowed()) {
          return $entity;
        }
      }
    }
  }

}
