<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\block_content\Plugin\Block\BlockContentBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class BlockContentBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return \Drupal::service('entity.repository')->getTranslationFromContext($this->getEntity());
  }

}
