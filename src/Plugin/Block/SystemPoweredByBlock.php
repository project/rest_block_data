<?php

namespace Drupal\rest_block_data\Plugin\Block;

use Drupal\system\Plugin\Block\SystemPoweredByBlock as ParentBlock;

/**
 * Override block class to include data for REST.
 */
class SystemPoweredByBlock extends ParentBlock implements BlockInterface {

  /**
   * {@inheritdoc}
   */
  public function buildToRest() {
    return $this->t('Powered by <a href=":poweredby">Drupal</a>', [':poweredby' => 'https://www.drupal.org'])->render();
  }

}
