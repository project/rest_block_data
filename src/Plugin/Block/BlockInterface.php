<?php

namespace Drupal\rest_block_data\Plugin\Block;

/**
 * Define required interface for the block class override.
 */
interface BlockInterface {

  /**
   * Return the block data for the REST.
   */
  public function buildToRest();

}
