<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\Core\Entity\EntityRepositoryInterface;

use Drupal\views\ResultRow;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * View result row normalizer.
 */
class ResultRowNormalizer extends NormalizerBase {
  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ResultRow::class;

  /**
   * The entity_repository.
   *
   * @var Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs object.
   */
  public function __construct(
    EntityRepositoryInterface $entityRepository
  ) {
    $this->entityRepository = $entityRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [];

    $entity = $this->entityRepository->getTranslationFromContext($object->_entity);
    $fields = $context['field'];

    if ($fields) {
      foreach ($fields as $fid => $field) {
        if ($entity->hasField($fid)) {
          $normalized[$fid] = $this->serializer->normalize($entity->get($fid), $format, $context);
        }
        else {
          $normalized[$fid] = $field->render($object);
        }
      }
    }
    else {
      $normalized = $this->serializer->normalize($entity, $format, $context);
    }

    return $normalized;
  }

}
