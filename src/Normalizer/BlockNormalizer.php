<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\serialization\Normalizer\ConfigEntityNormalizer;
use Drupal\block\BlockInterface;

use Drupal\rest_block_data\Plugin\Block\BlockInterface as RestBlockInterface;

/**
 * Normalizes block objects into an array structure.
 */
class BlockNormalizer extends ConfigEntityNormalizer {
  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = BlockInterface::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = parent::normalize($object, $format, $context);

    if (!$object->getPlugin() instanceof RestBlockInterface) {
      return $normalized;
    }

    $normalized['data'] = $this->serializer->normalize($object->getPlugin()->buildToRest(), $format, $context);

    return $normalized;
  }

}
