<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Breadcrumb Normalizer.
 */
class BreadcrumbNormalizer extends NormalizerBase {
  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = Breadcrumb::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [];

    foreach ($object->getLinks() as $link) {
      $normalized[] = $this->serializer->normalize($link, $format, $context);
    }

    return $normalized;
  }

}
