<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\Core\Link;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Core link Normalizer.
 */
class LinkNormalizer extends NormalizerBase {
  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = Link::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [
      'url' => $object->getUrl()->toString(),
      'title' => $this->serializer->normalize($object->getText(), $format, $context),
    ];

    return $normalized;
  }

}
