<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\views\ViewExecutable;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * View executable normalizer.
 */
class ViewExecutableNormalizer extends NormalizerBase {
  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ViewExecutable::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [];

    $object->execute();

    foreach ($object->result as $row) {
      $context['field'] = $object->getStyle()->usesFields() ? $object->field : NULL;
      $normalized['result'][] = $this->serializer->normalize($row, $format, $context);
    }

    $normalized['style'] = $this->serializer->normalize($object->getStyle(), $format, $context);
    $normalized['pager'] = $this->serializer->normalize($object->pager, $format, $context);

    return $normalized;
  }

}
