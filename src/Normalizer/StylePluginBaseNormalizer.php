<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Views style normalizer.
 */
class StylePluginBaseNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = StylePluginBase::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [
      'uses_fields' => $object->usesFields(),
    ];

    return $normalized;
  }

}
