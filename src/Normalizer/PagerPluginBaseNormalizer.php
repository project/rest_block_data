<?php

namespace Drupal\rest_block_data\Normalizer;

use Drupal\views\Plugin\views\pager\PagerPluginBase;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Views pager normalizer.
 */
class PagerPluginBaseNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = PagerPluginBase::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalized = [];

    if ($object->usePager()) {
      $normalized = [
        'total_items' => $object->getTotalItems(),
        'item_per_page' => $object->getItemsPerPage(),
        'total_pages' => ceil($object->getTotalItems() / $object->getItemsPerPage()),
      ];
    }

    return $normalized;
  }

}
